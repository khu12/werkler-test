#import os, sys
#from PyQt4.QtGui import *
#from PyQt4.QtCore import *
#from PyQt4.QtWebKit import *
#from lxml import html
#
#class Render(QWebPage):
#    def __init__(self, url):
#        self.app = QApplication(sys.argv)
#        QWebPage.__init__(self)
#        self.loadFinished.connect(self._loadFinished)
#        self.mainFrame().load(QUrl(url))
#        self.app.exec_()
#
#    def _loadFinished(self, result):
#        self.frame = self.mainFrame()
#        self.app.quit()
#
#
#def __exec_command__(command, filename):
#    os.system('{command} >> {filename}'.format(command=command, filename=filename))
#    print('COMMAND: {command}'.format(command=command))
#    if filename:
#        with open(filename, 'r') as f:
#            print(f.read())
#
#
#def test():
#    #__exec_command__('apt-get install -y python-qt4', 'python-qt4_install_result.txt')
#    #__exec_command__('dpkg -i python-qt4_4.11.2+dfsg-1_amd64.deb', 'python-qt4_install_result.txt')
#    url = 'https://ifconfig.co/json'
#    r = Render(url)
#    result = r.frame.toHtml()
#    print(result)
#
#
#if __name__ == '__main__':
#    test()
#
#import sys
#from PyQt4.QtGui import *
#from PyQt4.QtCore import *
#from PyQt4.QtWebKit import *
#from lxml import html
#
##Take this class for granted.Just use result of rendering.
#class Render(QWebPage):
#  def __init__(self, url):
#    self.app = QApplication(sys.argv)
#    QWebPage.__init__(self)
#    self.loadFinished.connect(self._loadFinished)
#    self.mainFrame().load(QUrl(url))
#    self.app.exec_()
#
#  def _loadFinished(self, result):
#    self.frame = self.mainFrame()
#    self.app.quit()
#
#url = 'http://pycoders.com/archive/'
#r = Render(url)
#result = r.frame.toHtml()
## This step is important.Converting QString to Ascii for lxml to process
#
## The following returns an lxml element tree
#archive_links = html.fromstring(str(result.toAscii()))
#print archive_links
#
## The following returns an array containing the URLs
#raw_links = archive_links.xpath('//div[@class="campaign"]/a/@href')
#print raw_links